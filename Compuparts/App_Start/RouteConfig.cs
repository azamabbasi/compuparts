﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Compuparts
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            

            routes.MapRoute(
               name: "Search",
               url: "Search-Results",
               defaults: new { controller = "Product", action = "Listing" });

            routes.MapRoute(
               name: "ProductDetail",
               url: "{name}-{ID}",
               defaults: new { controller = "Product", action = "ProductDetail" },
               constraints: new { ID = @"\d+" });

            routes.MapRoute(
                name: "contentPage",
                url: "{ContantPages}",
                defaults: new { controller = "ContentPage", action = "ShowContent" });

            routes.MapRoute(
               name: "Listingpage",
               url: "{Cate1}/{Cate2}/{Cate3}-{ID}",
               defaults: new { controller = "Product", action = "Listing" },
               constraints: new { ID = @"\d+" });

            

            routes.MapRoute(
                name: "Default",
                url: "{controller}",
                defaults: new { controller = "Index", action = "Index" }
            );
            routes.MapRoute(
               name: "generic",
               url: "{controller}/{action}",
               defaults: new { controller = "{controller}", action = "{action}" });
        }
    }
}
