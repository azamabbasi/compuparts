﻿using Compuparts.Models;
using Compuparts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class AjaxCallingController : Controller
    {
        // GET: AjaxCalling
        public ActionResult Quantitychecker(Quantitycheck qty)
        {
            int Quantity = new ProductModel().Quantitychecker(qty);
            return Content(Quantity.ToString());
        }
        public ActionResult Get3thLevelCategoryByParentID(int ID)
        {
            List<Category> lst = new CategoryModel().GetAllSubCategoryByParentID(ID);

            string html = "";
            if (lst.Count > 0)
            {
                foreach (var item in lst.OrderByDescending(x => x.SortOrder))
                {
                    html += "<tr id='" + item.ID + "' style='background-color:#e7f0e7' class='" + ID + "'><td>" + item.SortOrder + "</td><td>" + item.CategoryName + "</td>";
                    bool IsExist = new Compuparts.Models.CategoryModel().IsExist(item.ID);
                    if (IsExist == true)
                    {
                        html += " <td><a href='/Product/AddProduct?CategoryID=" + item.ID + "'>Add Product</a> - <a href='/Category/AddCategory?ID=" + item.ID + "'>Edit</a> - <a style='color: #C0C9D1'>Delete</a></td>";
                    }
                    else
                    {
                        IsExist = new Compuparts.Models.ProductModel().IsExist(item.ID);
                        if (IsExist == true)
                        {
                            html += "<td><a href='/Product/AddProduct?CategoryID=" + item.ID + "'>Add Product</a> - <a href='/Category/AddCategory?ID=" + item.ID + "'>Edit</a> - <a style='color: #C0C9D1'>Delete</a></td>";
                        }
                        else
                        {
                            html += "<td><a href='/Product/AddProduct?CategoryID=" + item.ID + "'>Add Product</a> - <a href='/Category/AddCategory?ID=" + item.ID + "'>Edit</a> - <a class='modal-container' data-toggle='modal' onclick='GetImageNameInHiddenBox(" + item.ID + ");' data-target='#modal-bottomfull'>Delete</a></td>";
                        }
                    }
                }
            }
            else
            {
                html += "<tr style='background-color:#f40d25' class='" + ID + "'><td>No Category Found</td> <td></td></tr>";
            }
            return Content(html);
        }
        public ActionResult Filters(string OrderBy)
        {
            string html = new ProductModel().Filters(OrderBy);
            return Content(html);
        }

        public ActionResult AddCart(CartData crt)
        {

            Guid guid = new Guid();
            Cart c = new Cart();
            c.ReadStatus = false;
            c.OrderStatus = "Pending";
            int CartID = 0;
            if (Session["GID"] == null || Session["GID"] == "00000000-0000-0000-0000-000000000000")
            {
                guid = Guid.NewGuid();
                Session["GID"] = guid;
                c.Guid = guid;
                CartID = new CartModel().Save(c);
            }
            else
            {
                guid = (Guid)Session["GID"];
                c = new CartModel().GetCartByGUID(guid.ToString());
                if (c == null)
                {
                    c.Guid = guid;
                    CartID = new CartModel().Save(c);
                }
                else
                {
                    CartID = c.ID;
                }
            }


            CartLine cl = new CartLine();
            cl.CartID = CartID;
            cl.ProductID = crt.ProductID;
            cl.ProductPrice = crt.ProductPrice;
            cl.Quantity = crt.Quantity;
            cl.TotalPrice = crt.Quantity * crt.ProductPrice;
            new CartLineModel().Save(cl);
            Product p = new ProductModel().GetProductByID(cl.ProductID);

            string HTML = "<div class='item' id='" + p.ID + "'><div class='image'><a href='#'><img src='/Resources/ProductImages/" + p.ProductImages.FirstOrDefault().Images + "' alt='" + p.Name + "' /></a></div><div class='productdesc'>" +
                           "<a href='"+p.Url+"-" + p.ID + "'>" + p.Name + "</a><div class='price'><span class='qty_" + p.ID + "'>" + crt.Quantity + "</span> x €<span class='prc_" + p.ID + "'>" + crt.ProductPrice + "</span></div></div><div class='removePrduct'><span class='fa fa-times' id='Rem_" + p.ID + "' onclick='Remove(" + p.ID + ")'></span></div></div>";

            Session["Cartid"] = CartID;
            return Content(HTML.ToString());
        }

        public ActionResult UpdateCart(CartData crt)
        {

            Guid guid = new Guid();
            if (Session["GID"] != null || Session["GID"] != "00000000-0000-0000-0000-000000000000")
            {
                guid = (Guid)Session["GID"];
            }
            Cart c = new CartModel().GetCartByGUID(guid.ToString());

            CartLine cl = new CartLineModel().GetCartLineByCartIDAndProductID(c.ID, crt.ProductID);


            CartLine cll = new CartLine();
            cll.ID = cl.ID;
            cll.CartID = c.ID;
            cll.ProductID = crt.ProductID;
            cll.ProductPrice = crt.ProductPrice;
            cll.Quantity = cl.Quantity + crt.Quantity;
            cll.TotalPrice = cl.ProductPrice * cll.Quantity;
            new CartLineModel().Update(cll);

            string valuesReturn = crt.Quantity + "-" + crt.ProductID;
            return Content(valuesReturn.ToString());
        }

        public ActionResult RemoveCartLine(int ID)
        {
            new CartLineModel().Remove(ID);

            return null;
        }


    }
}