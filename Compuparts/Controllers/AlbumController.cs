﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class AlbumController : Controller
    {
        public ActionResult AddAlbum()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Album Edit = new AlbumModel().GetAlbumByID(ID);
                ViewBag.Name = Edit.Name;
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddAlbum(Album albm, string HdnName)
        {
            if (albm.Description != "" && albm.Description != null)
            {
                if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
                {
                    albm.ID = Convert.ToInt16(Request.QueryString["ID"]);
                    new AlbumModel().Update(albm);
                    if (albm.Name != HdnName)
                    {
                        string pathToCreate = "~/Resources/Albums/" + HdnName;
                        if (Directory.Exists(Server.MapPath(pathToCreate)))
                        {
                            System.IO.Directory.Move(Server.MapPath(" /Resources/Albums/" + HdnName), Server.MapPath("/Resources/Albums/" + albm.Name));
                        }
                    }
                    TempData["AlertTask"] = "Record Successfully Updated";
                }
                else
                {
                    new AlbumModel().Save(albm);
                    TempData["AlertTask"] = "Record Successfully Saved";
                }
                return RedirectToAction("ManageAlbum");
            }
            else
            {
                TempData["AlertTask"] = "Please Enter the Description";
                return View();
            }
        }

        public ActionResult ManageAlbum()
        {
            List<Album> lst = new AlbumModel().GetAllAlbum();
            return View(lst);
        }

        public ActionResult Delete(int ID)
        {
            Album Edit = new AlbumModel().GetAlbumByID(ID);
            new AlbumImagesModel().DeleteByAlbumID(ID);
            new AlbumModel().Delete(ID);
            string pathToCreate = "~/Resources/Albums/" + Edit.Name;
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                System.IO.Directory.Delete(Server.MapPath(pathToCreate), true);
            }
            TempData["AlertTask"] = "Record Successfully Delete";
            return RedirectToAction("ManageAlbum");
        }
	}
}