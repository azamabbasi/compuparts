﻿using Compuparts.Models.Function;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Compuparts.Models;
using System.IO;

namespace Compuparts.Controllers
{
    public class AlbumImagesController : Controller
    {
        public ActionResult AddAlbumImages()
        {
            var lst = new GernalFunction().AlbumList();
            ViewBag.AlbumList = lst;
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Album Edit = new AlbumModel().GetAlbumByID(ID);
                List<AlbumImage> lstImages = new AlbumImagesModel().GetAlbumImageByAlbumID(ID);
                if (lst.Count() > 0)
                {
                    foreach (var item in lstImages)
                    {
                        ViewBag.img += "<li><img src='/Resources/Albums/" + item.Album.Name + "/" + item.Image + "' width='76px' height='54px' style='float: left; height:54px; width=76px; margin: 0px; ' /><span onclick='ImageRemove(this);' class='close'>X</span><textarea id='TextArea1' rows='2' cols='20' style='width: 75%;'>" + item.Description + "</textarea></li>";
                    }
                }
                ViewBag.AlbumName = Edit.Name;
                ViewBag.AlbumID = Edit.ID;
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult AddAlbumImages(string hdnImages, string hdn_albumname, int AlbumID)
        {
            var lst = new GernalFunction().AlbumList();
            ViewBag.AlbumList = lst;

            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                new AlbumImagesModel().DeleteByAlbumID(AlbumID);

                #region CreateDirectory
                string pathToCreate = "~/Resources/Albums/" + hdn_albumname;
                if (Directory.Exists(Server.MapPath(pathToCreate)))
                {
                }
                else
                {
                    Directory.CreateDirectory(Server.MapPath(pathToCreate));
                }
                #endregion

                SaveImages(hdnImages, AlbumID, hdn_albumname);
            }
            else
            {
                #region CreateDirectory
                string pathToCreate = "~/Resources/Albums/" + hdn_albumname;
                if (Directory.Exists(Server.MapPath(pathToCreate)))
                {
                }
                else
                {
                    Directory.CreateDirectory(Server.MapPath(pathToCreate));
                }
                #endregion

                SaveImages(hdnImages, AlbumID, hdn_albumname);
            }
            return RedirectToAction("ManageAlbum", "Album");
        }

        public ActionResult ManageAlbumImages()
        {
            List<Album> lst = new AlbumModel().GetAllAlbum();
            return View(lst);
        }

        public void SaveImages(string hdnImages, int AlbumID, string hdn_albumname)
        {
            #region Album Images Saved
            AlbumImage img = new AlbumImage();
            if (hdnImages != "")
            {
                List<string> Imglst = new List<string>();
                string[] stringSeparators = new string[] { "#####" };
                Imglst = hdnImages.Split(stringSeparators, StringSplitOptions.None).ToList();
                int a = Imglst.Count();
                Imglst.RemoveAt(a - 1);
                string Images = null;
                foreach (var item in Imglst)
                {
                    if (!item.Contains("/Resources"))
                    {
                        string[] imgnDescSeparators = new string[] { "/*/*/*" };
                        List<string> ImgnDesc = item.Split(imgnDescSeparators, StringSplitOptions.None).ToList();
                        Images += GernalFunction.ImageCroping(ImgnDesc[0], "/Resources/Albums/" + hdn_albumname + "/", false);
                        img.AlbumID = Convert.ToInt16(AlbumID);
                        img.Image = Images;
                        img.Description = ImgnDesc[1];
                        new AlbumImagesModel().Save(img);
                        Images = null;
                    }
                    else
                    {
                        string[] imgnDescSeparators = new string[] { "/*/*/*" };
                        List<string> ImgnDesc = item.Split(imgnDescSeparators, StringSplitOptions.None).ToList();
                        Images += Path.GetFileName(ImgnDesc[0]);
                        img.AlbumID = Convert.ToInt16(AlbumID);
                        img.Image = Images;
                        img.Description = ImgnDesc[1];
                        new AlbumImagesModel().Save(img);
                        Images = null;

                    }
                }

            }
            else
            {

            }
            #endregion
        }
    }
}