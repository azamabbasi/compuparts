﻿using Compuparts.Models;
using Compuparts.Models.Function;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class BrandController : Controller
    {
        // GET: Brand
        public ActionResult AddBrand()
        {
            var lst = new GernalFunction().BrandList();
            ViewBag.DDLCate = lst;
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Brand brand = new BrandModel().GetBrandByID(ID);
                brand.Image = "/Resources/Brand/" + brand.Image;
                return View(brand);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult AddBrand(Brand brnd)
        {
            var lst = new GernalFunction().BrandList();
            ViewBag.DDLCate = lst;
            if (brnd.Name != null && brnd.Name != "")
            {
                if (brnd.Image != null)
                {
                    if (!brnd.Image.Contains("Resources/Brand"))
                    {
                        brnd.Image = GernalFunction.ImageCroping(brnd.Image, "/Resources/Brand/", false);
                    }
                    else
                    {
                        brnd.Image = Path.GetFileName(brnd.Image);
                    }
                }
                else
                {
                    brnd.Image = "NoImage.jpg";
                }
                if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
                {
                    brnd.ID = Convert.ToInt16(Request.QueryString["ID"]);
                    new BrandModel().Update(brnd);
                }
                else
                {
                    new BrandModel().Save(brnd);
                }
            }
            return RedirectToAction("ManageBrand");
        }

        public ActionResult ManageBrand()
        {
            List<Brand> brand = new BrandModel().GetAll();
            return View(brand);
        }

        public ActionResult Delete(int ID)
        {
            new BrandModel().Delete(ID);
            return RedirectToAction("ManageBrand");
        }
    }
}