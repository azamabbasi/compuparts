﻿using Compuparts.Models;
using Compuparts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class CartController : Controller
    {
        //
        // GET: /Cart/

        public ActionResult ManageOrder()
        {
            List<Cart> c = new CartModel().GetAll();
            return View(c);
        }

        public ActionResult OrderDetails()
        {
            OrderViewModel ord = new OrderViewModel();
            new CartModel().ReadStatus(ord.CartId);
            return View(ord);
        }


        public ActionResult UpdateStatus(string Status, string Cart_Comments, string Cart_ID)
        {
            new CartModel().UpdateStatus(Convert.ToInt32(Cart_ID), Cart_Comments, Status);
            return RedirectToAction("ManageOrder");
        }

        

    }
}
