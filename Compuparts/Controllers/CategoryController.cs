﻿using Compuparts.Models;
using Compuparts.Models.Function;
using Compuparts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class CategoryController : Controller
    {
        public ActionResult AddCategory()
        {

            AddCategoryViewModel catelist = new AddCategoryViewModel();
            ViewBag.Url = HttpContext.Request.UrlReferrer.ToString();
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                catelist.Category = new CategoryModel().GetbyID(ID);
                return View(catelist);
            }
            return View(catelist);
        }

        [HttpPost]
        public ActionResult AddCategory(AddCategoryViewModel cate, string DDLcategory, string PreUrl)
        {
            if (!string.IsNullOrEmpty(DDLcategory))
            {
                cate.Category.ParentCategoryID = Convert.ToInt32(DDLcategory);
            }
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                cate.Category.ID = Convert.ToInt16(Request.QueryString["ID"]);
                cate.Category.Category1 = null;
                cate.Category.Categories1 = null;
                new CategoryModel().Update(cate.Category);
            }
            else
            {
                if (cate.Category.CategoryName.Contains(","))
                {
                    Category MultipleName = new Category();
                    MultipleName.ParentCategoryID = cate.Category.ParentCategoryID;

                    string[] arr = cate.Category.CategoryName.Split(',');
                    short a = 1;
                    foreach (var item in arr)
                    {
                        MultipleName.CategoryName = item;
                        MultipleName.SortOrder = a;
                        new CategoryModel().Save(MultipleName);
                        a++;
                    }
                }
                else
                {
                    new CategoryModel().Save(cate.Category);
                }
            }
            PreUrl = PreUrl.Replace("http://localhost:23110", "");
            PreUrl = PreUrl.Replace("http://compuparts.ie", "");
            if (!string.IsNullOrEmpty(PreUrl))
            {
                return Redirect(PreUrl);
            }
            else
            {
                return View();
            }
        }

        public ActionResult ManageCategory()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                List<Category> cate = new CategoryModel().GetAllSubCategoryByParentID(Convert.ToInt32(Request.QueryString["ID"]));
                return View(cate);
            }
            return View();
        }

        public ActionResult ManageMainCategory()
        {
            List<Category> cate = new CategoryModel().GetAllMainCategory();
            return View(cate);
        }

        public ActionResult Delete(int ID)
        {
            new CategoryModel().Delete(ID);
            return RedirectToAction("ManageCategory");
        }
    }
}