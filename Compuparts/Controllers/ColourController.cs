﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class ColourController : Controller
    {
        // GET: Colour
        public ActionResult AddColour()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Colour col = new ColourModel().GetbyID(ID);
                return View(col);
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult AddColour(Colour col)
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                col.ID = Convert.ToInt16(Request.QueryString["ID"]);
                new ColourModel().Update(col);
            }
            else
            {
                new ColourModel().Save(col);
            }
            return RedirectToAction("ManageColour");
        }

        public ActionResult ManageColour()
        {
            List<Colour> len = new ColourModel().GetAll();
            return View(len);
        }

        public ActionResult Delete(int ID)
        {
            new ColourModel().Delete(ID);
            return RedirectToAction("ManageColour");
        }
    }
}