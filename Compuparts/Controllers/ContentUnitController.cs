﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class ContentUnitController : Controller
    {
        public ActionResult AddContentUnit()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                ContentUnit Edit = new ContentUnitModel().GetContentUnitByID(ID);
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddContentUnit(ContentUnit unit, string hdn_UserID)
        {
            if (unit.Description != "" && unit.Description != null)
            {
                unit.Date = DateTime.Now;
                unit.UserID = Convert.ToInt32(hdn_UserID);
                if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
                {
                    unit.ID = Convert.ToInt32(Request.QueryString["ID"]);
                    new ContentUnitModel().Update(unit);
                    TempData["AlertTask"] = "Record Successfully Update";
                }
                else
                {
                    new ContentUnitModel().Save(unit);
                    TempData["AlertTask"] = "Record Successfully Saved";
                }
                return RedirectToAction("ManageContentUnit");
            }
            else
            {
                TempData["AlertTask"] = "Please Enter the Description";
                return View();
            }
        }

        public ActionResult ManageContentUnit()
        {
            List<ContentUnit> lst = new ContentUnitModel().GetAllContentUnit();
            return View(lst);
        }

        public void Delete(int ID)
        {
            new ContentUnitModel().Delete(ID);
            TempData["AlertTask"] = "Record Successfully Delete";
            Response.Redirect("ManageContentUnit");
        }
	}
}