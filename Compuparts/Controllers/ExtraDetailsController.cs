﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class ExtraDetailsController : Controller
    {
        // GET: ExtraDetails
        public ActionResult AddExtraDetails()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                ExtraDetail col = new ExtraDetailsModel().GetExtraDetailsByID(ID);
                return View(col);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddExtraDetails(ExtraDetail col)
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                col.ID = Convert.ToInt16(Request.QueryString["ID"]);
                new ExtraDetailsModel().Update(col);
            }
            else
            {
                new ExtraDetailsModel().Save(col);
            }
            return RedirectToAction("ManageExtraDetail");
        }

        public ActionResult ManageExtraDetail()
        {
            List<ExtraDetail> len = new ExtraDetailsModel().GetAll();
            return View(len);
        }

        public ActionResult Delete(int ID)
        {
            new ExtraDetailsModel().Delete(ID);
            return RedirectToAction("ManageExtraDetail");
        }
    }
}