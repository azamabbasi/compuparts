﻿using Compuparts.Models;
using Compuparts.Models.Function;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class ExtraImageController : Controller
    {
        //
        // GET: /ExtraImage/

        public ActionResult AddExtraImages()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                ExtraImage Edit = new ExtraImagesModel().GetExtraImageByID(ID);
                Edit.Image = "/Resources/ExtraImages/" + Edit.Image;
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddExtraImages(HttpPostedFileBase[] files,ExtraImage Extraimg)
        {
            ExtraImage img = new ExtraImage();
            img.Url = Extraimg.Url;
            foreach (HttpPostedFileBase file in files)
            {
                //if (file != null && string.IsNullOrEmpty(Extraimg.Images))
                //{
                //    /*Geting the file name*/
                //    string filename = System.IO.Path.GetFileName(file.FileName);
                //    string ext = System.IO.Path.GetExtension(file.FileName);
                //    /*Saving the file in server folder*/
                //    filename = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "-" + ext;
                //    file.SaveAs(Server.MapPath("~/Resources/Extraimages/" + filename));
                //    string filepathtosave = "Resources/Extraimages/" + filename;
                //    /*HERE WILL BE YOUR CODE TO SAVE THE FILE DETAIL IN DATA BASE*/
                //    img.Images = filename;
                //    new ExtraImagesModel().Save(img);
                //}
                //else
                if (file != null)
                {
                    /*Geting the file name*/
                    string filename = System.IO.Path.GetFileName(file.FileName);
                    string ext = System.IO.Path.GetExtension(file.FileName);
                    /*Saving the file in server folder*/
                    filename = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "-" + ext;
                    file.SaveAs(Server.MapPath("~/Resources/Extraimages/" + filename));
                    string filepathtosave = "Resources/Extraimages/" + filename;
                    /*HERE WILL BE YOUR CODE TO SAVE THE FILE DETAIL IN DATA BASE*/
                    img.Image = filename;
                    img.ID = Extraimg.ID;
                    new ExtraImagesModel().Update(img);
                }
                else
                {
                    string filename = System.IO.Path.GetFileName(Extraimg.Image);
                    string ext = System.IO.Path.GetExtension(Extraimg.Image);
                    img.Image = filename;
                    img.ID = Extraimg.ID;
                    new ExtraImagesModel().Update(img);
                }
            }
            
            return RedirectToAction("ManageExtraImage");
            
        }

        public ActionResult ManageExtraImage()
        {
            List<ExtraImage> lst = new ExtraImagesModel().GetAllExtraImage();
            return View(lst);
        }

    }
}
