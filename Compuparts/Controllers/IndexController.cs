﻿using Compuparts.Models;
using Compuparts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class IndexController : Controller
    {
        // GET: Index
        public ActionResult Index()
        {
            IndexPageViewModel index = new IndexPageViewModel();
            return View(index);
        }

        public ActionResult Aknowledgement(string Message, string Heading)
        {

            Session["SGuid"] = null;
            if (!string.IsNullOrEmpty(Heading))
            {
                ViewBag.Heading = Heading;
            }
            else
            {
                Heading = "Thank you ";
            }
            ViewBag.Heading = Heading;
            ViewBag.Message = Message;
            return View();
        }
        public ActionResult cancellation()
        {
            string msg = "Your payment process is cancelled! Please try Again";
            string cancel = "Payment Failed !";
            return RedirectToAction("Aknowledgement", "Index", new { Message = msg, Heading = cancel });
        }

        public ActionResult success()
        {
            int CartID = Convert.ToInt32(Session["CartID"]);
            new CartModel().OrderConfirm(CartID);
            Session["SGuid"] = null;

            string msg = "Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account to view details of this transaction.";
            return RedirectToAction("Aknowledgement", "Index", new { Message = msg });
        }
    }
}