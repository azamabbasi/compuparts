﻿using Compuparts.Models;
using Compuparts.Models.Function;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class MainSlidersController : Controller
    {
        public ActionResult AddMainSlider()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                MainSlider Edit = new MainSliderModel().GetMainSliderByID(ID);
                Edit.Image = "/Resources/MainSlider/" + Edit.Image;
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddMainSlider(MainSlider slider)
        {
            if (slider.Image != null)
            {
                if (!slider.Image.Contains("Resources/News"))
                {
                    slider.Image = GernalFunction.ImageCroping(slider.Image, "/Resources/MainSlider/", false);
                }
                else
                {
                    slider.Image = Path.GetFileName(slider.Image);
                }
            }
            else
            {
                slider.Image = "NoImage.jpg";
            }


            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                slider.ID = Convert.ToInt16(Request.QueryString["ID"]);
                new MainSliderModel().Update(slider);
                TempData["AlertTask"] = "Record Successfully Update";
            }
            else
            {
                new MainSliderModel().Save(slider);
                TempData["AlertTask"] = "Record Successfully Saved";
            }
            return RedirectToAction("ManageMainSlider");

        }

        public ActionResult ManageMainSlider()
        {
            List<MainSlider> lst = new MainSliderModel().GetAllMainSlider();
            return View(lst);
        }

        public void Delete(int ID)
        {
            new MainSliderModel().Delete(ID);
            TempData["AlertTask"] = "Record Successfully Deleted";
            Response.Redirect("ManageMainSlider");
        }
	}
}