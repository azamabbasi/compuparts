﻿using Compuparts.Models;
using Compuparts.Models.Function;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class NewsController : Controller
    {
        public ActionResult AddNews()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                News Edit = new NewsModel().GetNewsByID(ID);
                Edit.Image = "/Resources/News/" + Edit.Image;
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNews(News news)
        {
            if (news.Description != null && news.Description != "")
            {
                if (news.Image != null)
                {
                    if (!news.Image.Contains("Resources/News"))
                    {
                        news.Image = GernalFunction.ImageCroping(news.Image, "/Resources/News/", false);
                    }
                    else
                    {
                        news.Image = Path.GetFileName(news.Image);
                    }
                }
                else
                {
                    news.Image = "NoImage.jpg";
                }


                if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
                {
                    news.ID = Convert.ToInt32(Request.QueryString["ID"]);
                    new NewsModel().Update(news);
                    TempData["AlertTask"] = "Record Successfully Update";
                }
                else
                {
                    new NewsModel().Save(news);
                    TempData["AlertTask"] = "Record Successfully Saved";
                }
                return RedirectToAction("ManageNews");
            }
            else
            {
                TempData["AlertTask"] = "Please Enter the Description";
                return View();
            }
        }

        public ActionResult ManageNews()
        {
            List<News> lst = new NewsModel().GetAllNews();
            return View(lst);
        }

        public void Delete(int ID)
        {
            new NewsModel().Delete(ID);
            TempData["AlertTask"] = "Record Successfully Deleted";
            Response.Redirect("ManageNews");
        }
    }
}