﻿using Compuparts.Models;
using Compuparts.Models.Function;
using Compuparts.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult AddProduct()
        {
            var lst = new GernalFunction().ExtraDetailList();
            ViewBag.DDLExtraDetails = lst;
            ProductViewModel pd = new ProductViewModel();

            #region For Update Product

            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                pd.Product = new ProductModel().GetProductByID(ID);
                #region Images
                foreach (var item in pd.Product.ProductImages)
                {
                    pd.ImagesHtml += "<li><img src='/Resources/ProductImages/" + item.Images + "' width='200px' height='200px' style='float: left; height:200px; width=200px; margin: 0px; ' /><span onclick='ImageRemove(this);' class='close'>X</span></li>";
                }
                #endregion
            }

            #endregion

            #region For Add Similar Product

            if (Convert.ToInt32(Request.QueryString["SimilarProductID"]) != 0 && Request.QueryString["SimilarProductID"] != null && Request.QueryString["SimilarProductID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["SimilarProductID"]);
                pd.Product = new ProductModel().GetProductByID(ID);
                #region Images
                foreach (var item in pd.Product.ProductImages)
                {
                    pd.ImagesHtml += "<li><img src='/Resources/ProductImages/" + item.Images + "' width='200px' height='200px' style='float: left; height:200px; width=200px; margin: 0px; ' /><span onclick='ImageRemove(this);' class='close'>X</span></li>";
                }
                #endregion
            }

            #endregion

            return View(pd);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddProduct(ProductViewModel productMod, string hdnImages, HttpPostedFileBase[] files, string DDLcategory)
        {
            var lst = new GernalFunction().ExtraDetailList();
            ViewBag.DDLExtraDetails = lst;
            if (!string.IsNullOrEmpty(DDLcategory))
            {
                productMod.Product.CategoryID = Convert.ToInt32(DDLcategory);
            }
            if (productMod.ProductId > 0)
            {
                #region Update

                Category cat = new CategoryModel().GetbyID(productMod.Product.CategoryID);
                productMod.Product.ID = Convert.ToInt32(Request.QueryString["ID"]);
                new ProductImagesModel().DeleteByProductID(productMod.Product.ID);
                if (hdnImages != null && hdnImages != "")
                {
                    SaveImages(hdnImages, productMod.Product.ID);
                }
                else
                {
                    ProductImage img = new ProductImage();
                    img.ProductID = productMod.Product.ID;
                    img.Images = "NoImage.png";
                    new ProductImagesModel().Save(img);
                }
                new ProductModel().Update(productMod.Product);
                new ProductRegionsModel().DeleteProductRegionByProductID(productMod.Product.ID);

                #endregion
            }
            else if (Convert.ToInt32(Request.QueryString["SimilarProductID"]) != 0 && Request.QueryString["SimilarProductID"] != null && Request.QueryString["SimilarProductID"] != "")
            {
                #region Add Similar Product
                productMod.Product.ID = 0;
                Category cat = new CategoryModel().GetbyID(productMod.Product.CategoryID);
                int ProductSavedID = new ProductModel().Save(productMod.Product);
                if (ProductSavedID > 0)
                {
                    if (hdnImages != null && hdnImages != "")
                    {
                        SaveImages(hdnImages, ProductSavedID);
                    }
                    else
                    {
                        ProductImage img = new ProductImage();
                        img.ProductID = productMod.Product.ID;
                        img.Images = "NoImage.png";
                        new ProductImagesModel().Save(img);
                    }
                }
                #endregion
            }
            else
            {
                #region Saved
                Category cat = new CategoryModel().GetbyID(productMod.Product.CategoryID);
                int ProductSavedID = new ProductModel().Save(productMod.Product);
                if (ProductSavedID > 0)
                {
                    if (!string.IsNullOrEmpty(hdnImages))
                    {
                        SaveImages(hdnImages, ProductSavedID);
                    }
                    else
                    {
                        ProductImage img =  new ProductImage();
                        img.ProductID  =  ProductSavedID;
                        img.Images = "NoImage.png";
                        new ProductImagesModel().Save(img);
                    }
                }
                

                #endregion
            }
            ProductRegion len = new ProductRegion();
            len.ProductID = productMod.Product.ID;
            foreach (var item in productMod.RegionList.Where(c => c.IsSelected == true))
            {
                len.RegionID = Convert.ToInt32(item.ID);
                new ProductRegionsModel().Save(len);
            }
            return RedirectToAction("ManageProduct");
        }

        public void SaveRegion()
        {

        }

        public void SaveImages(string hdnImages, int SavedProductID)
        {
            #region Product Images Saved
            ProductImage img = new ProductImage();
            if (hdnImages != "")
            {
                List<string> Imglst = new List<string>();
                string[] stringSeparators = new string[] { "#####" };
                Imglst = hdnImages.Split(stringSeparators, StringSplitOptions.None).ToList();
                int a = Imglst.Count();
                Imglst.RemoveAt(a - 1);
                string Images = null;
                foreach (var item in Imglst)
                {
                    if (!item.Contains("/Resources"))
                    {
                        Images += GernalFunction.ImageCroping(item, "/Resources/ProductImages/", false);
                        Images += ",";
                    }
                    else
                    {
                        Images += Path.GetFileName(item);
                        Images += ",";
                    }
                }

                List<string> splitimgs = new List<string>();
                splitimgs = Images.Split(',').ToList();
                int b = splitimgs.Count();
                splitimgs.RemoveAt(b - 1);
                foreach (var item in splitimgs)
                {
                    img.ProductID = SavedProductID;
                    img.Images = item.ToString();
                    new ProductImagesModel().Save(img);
                }
            }
            #endregion
        }

        public ActionResult ManageProduct()
        {
            var lst = new ProductModel().GetAll();
            return View(lst);
        }

        public ActionResult AddProductQuantity()
        {

            AddProductQuantityViewModel modl = new AddProductQuantityViewModel();
            return View(modl);
        }

        [HttpPost]
        public ActionResult AddProductQuantity(AddProductQuantityViewModel Qtymodel)
        {
            string url = this.Request.UrlReferrer.AbsolutePath;
            if (Qtymodel.ProductQty.ProductID > 0)
            {
                ProductQuantity Prodqty = new ProductQuantity();
                Prodqty.ProductID = Qtymodel.ProductQty.ProductID;
                url = url + "?ID=" + Qtymodel.ProductQty.ProductID;
                int i = 0;
                foreach (var item in Qtymodel.ListOfProductQuantaties)
                {
                    Prodqty.SizeID = item.SizeID;
                    Prodqty.ColourID = item.ColourID;
                    Prodqty.Quantity = item.Quantity;
                    if (Prodqty.ColourID != null && Prodqty.Quantity != null)
                    {
                        new ProductQuantityModel().Save(Prodqty);
                        i++;
                    }
                    else if (Prodqty.SizeID != null && Prodqty.Quantity != null)
                    {
                        new ProductQuantityModel().Save(Prodqty);
                        i++;
                    }
                }
                if (i == 0)
                {
                    TempData["message"] = "Data Couldn't Save! Please Select (Size and Quantity) OR (Colour and Quanitity) ";
                }

            }
            else
            {
                TempData["message"] = "Data Couldn't Save! Please Select Product ";
            }
            return Redirect(url);
        }

        public ActionResult ManageProductQuantity()
        {
            List<ProductQuantity> lst = new ProductQuantityModel().GetAll();
            return View(lst);
        }

        public ActionResult SoftDelete(int ID)
        {
            var user = new Product()
            {
                ID = ID,
                Is_Deleted = true
            };
            using (var dbMember = new compuparts_dbEntities())
            {
                dbMember.Products.Attach(user);
                dbMember.Entry(user).Property(x => x.Is_Deleted).IsModified = true;
                dbMember.SaveChanges();
            }
            return RedirectToAction("ManageProduct");
        }

        public ActionResult ProductDetail()
        {
            ProductDetailViewModel pdvm = new ProductDetailViewModel();
            return View(pdvm);
        }

        public ActionResult Listing()
        {
            ListingPageViewModel mod = new ListingPageViewModel("");
            return View(mod);
        }
        
        
        [HttpPost]
        [ActionName("Listing")]
        public ActionResult Search(string keyword)
        {
            ListingPageViewModel mod = new ListingPageViewModel(keyword);
            return View(mod);
        }

        public ActionResult ShoppingCart()
        {
            ShoppingCartViewModel Spcart = new ShoppingCartViewModel();
            return View(Spcart);
        }

        public ActionResult remove()
        {
            if (Request.QueryString["id"] != null)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                new CartLineModel().Remove(id);
                return RedirectToAction("ShoppingCart");
            }
            return RedirectToAction("index");
        }


        public ActionResult CheckOut(ShoppingCartViewModel mod, string hdnFinal, int Cart_ID, int DDLCountry)
        {
            try
            {
                mod.billinfo.CartID = Cart_ID;
                mod.billinfo.CountryID = DDLCountry;
                new BillingInfoModel().Save(mod.billinfo);
                new CartModel().UpdateDateAndPrice(Convert.ToInt32(Cart_ID),DateTime.Now.ToString(), hdnFinal);
                string scrpt = "";
                string strForm = "";

                #region PayPal

                scrpt = "<script type='text/javascript' language='javascript'>document.forms['PayPalForm'].submit();</script>";

                strForm = "";
                strForm = strForm + string.Format("<form name=\"PayPalForm\" method=\"POST\" action=\"{0}\" >", System.Configuration.ConfigurationManager.AppSettings["PayPalSubmitUrl"].ToString());
                strForm = strForm + string.Format("<input type=\"hidden\" value=\"_xclick\" name=\"cmd\">");
                strForm = strForm + string.Format("<input type=\"hidden\" value=\"{0}\" name=\"business\">", System.Configuration.ConfigurationManager.AppSettings["paypalemail"].ToString());
                strForm = strForm + string.Format("<input type=\"hidden\" value=\"EUR\" name=\"currency_code\">");
                strForm = strForm + string.Format("<input type=\"hidden\" value=\"{0}\" name=\"return\">", System.Configuration.ConfigurationManager.AppSettings["paypalSuccessURL"].ToString());
                strForm = strForm + string.Format("<input type=\"hidden\" value=\"{0}\" name=\"cancel_return\">", System.Configuration.ConfigurationManager.AppSettings["paypalFailedURL"].ToString());
                strForm = strForm + string.Format("<input type=\"hidden\" value=\"2\" name=\"rm\">");
                strForm = strForm + string.Format(" <input type=\"hidden\" name=\"amount\" value=\"{0}\" /> ", hdnFinal);
                //strForm = strForm + string.Format("<input type=\"hidden\" nam=\"item_name\" value=\"{0)\"/>", ord.listCartItems[0].prod.product_name.ToString());
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"item_number\" value=\"{0}\" />", mod.CartProducts.Count.ToString());
                //strForm = strForm + string.Format("<input type=\"hidden\" name=\"item_name\" value=\"{0}\" />", ord.listCartItems[0].prod.product_name.ToString());

                strForm = strForm + string.Format("<input type=\"hidden\" name=\"name\" value=\"{0}\">", mod.billinfo.Fullname);
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"address1\" value=\"{0}\">", mod.billinfo.Address);
                //strForm = strForm + string.Format("<input type=\"hidden\" name=\"town\" value=\"{0}\">", "");
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"postcode\" value=\"{0}\">", mod.billinfo.PostalCode);
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"country\" value=\"GB\">");
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"tel\" value=\"{0}\">", mod.billinfo.Phone);
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"address2\" value=\"{0}\">", mod.billinfo.Address);
                strForm = strForm + string.Format("<input type=\"hidden\" name=\"email\" value=\"{0}\">", mod.billinfo.Email);
                //strForm = strForm + string.Format("<input type=\"hidden\" name=\"town\" value=\"{0}\">", 0);
                //strForm = strForm + string.Format("<input type=\"hidden\" value=\"notify-validate\" name=\"cmd\">");
                strForm = strForm + "</form>";
                strForm = strForm + scrpt;

                #endregion

                Session.Add("strForm", strForm);
                Session["Price"] = hdnFinal;
            }
            catch (Exception ex)
            {
                Session.Add("strForm", "Sorry! Some problem occurred.");
            }
            return View();
        }


    }
}