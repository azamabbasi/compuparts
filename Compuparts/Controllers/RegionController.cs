﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class RegionController : Controller
    {
        // GET: Region
        public ActionResult AddRegion()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Region col = new RegionModel().GetByID(ID);
                return View(col);
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult AddRegion(Region col)
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                col.ID = Convert.ToInt16(Request.QueryString["ID"]);
                new RegionModel().Update(col);
            }
            else
            {
                new RegionModel().Save(col);
            }
            return RedirectToAction("ManageRegion");
        }

        public ActionResult ManageRegion()
        {
            List<Region> len = new RegionModel().GetAll();
            return View(len);
        }

        public ActionResult Delete(int ID)
        {
            new RegionModel().Delete(ID);
            return RedirectToAction("ManageRegion");
        }
    }
}