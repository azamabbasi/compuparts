﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class SizeController : Controller
    {
        // GET: Size
        public ActionResult AddSize()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Size len = new SizeModel().GetbyID(ID);
                return View(len);
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult AddSize(Size sz)
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                sz.ID = Convert.ToInt16(Request.QueryString["ID"]);
                new SizeModel().Update(sz);
            }
            else
            {
                new SizeModel().Save(sz);
            }
            return RedirectToAction("ManageSize");
        }

        public ActionResult ManageSize()
        {
            List<Size> sz = new SizeModel().GetAll();
            return View(sz);
        }

        public ActionResult Delete(int ID)
        {
            new SizeModel().Delete(ID);
            return RedirectToAction("ManageSize");
        }
    }
}