﻿using EmailFunctions;
using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class SubscriberController : Controller
    {
        public ActionResult AddSubscriber()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Subscriber Edit = new SubscribersModel().GetSubscriberByID(ID);
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddSubscriber(string Email)
        {
            Subscriber sub = new Subscriber();
            sub.Email = Email;
            sub.Date = DateTime.Now;
            new SubscribersModel().Save(sub);
            return null;
        }

        public ActionResult ManageSubscriber()
        {
            List<Subscriber> lst = new SubscribersModel().GetAllSubscriber();
            return View(lst);
        }

        public void Delete(int ID)
        {
            new SubscribersModel().Delete(ID);
            TempData["AlertTask"] = "Record Successfully Deleted";
            Response.Redirect("ManageSubscriber");
        }

        public ActionResult SendEmailToSubscriber()
        {
            List<Subscriber> lst = new SubscribersModel().GetAllSubscriber();
            ViewBag.Count = lst.Count();
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SendEmailToSubscriber(string Name, string EmailSubject, string Email, string EmailContent)
        {
            string Subject = EmailSubject;
            string Emailfrom = Email;
            string Title = Name;
            string EmailMessage = EmailContent;
            if (EmailContent != "" && EmailContent != null)
            {
                List<Subscriber> lst = new SubscribersModel().GetAllSubscriber();
                foreach (var item in lst)
                {
                    try
                    {
                        EmailSender.EmailSend(Subject, Emailfrom, Title, item.Email, EmailMessage);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                TempData["AlertTask"] = "Emails Successfully Sent to "+lst.Count()+ "Subscribers";
               
                 return RedirectToAction("DashboardIndex", "user");  
            }
            else
            {
                TempData["AlertTask"] = "Email cannot sent! Please fill all fields";
                return View();
            }
        }
    }
}