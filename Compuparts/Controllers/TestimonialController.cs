﻿using Compuparts.Models;
using Compuparts.Models.Function;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Controllers
{
    public class TestimonialController : Controller
    {
        public ActionResult AddTestimonial()
        {
            if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
            {
                int ID = Convert.ToInt32(Request.QueryString["ID"]);
                Testimonial Edit = new TestimonialModel().GetTestimonialsByID(ID);
                Edit.Image = "/Resources/Testimonial/" + Edit.Image;
                return View(Edit);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddTestimonial(Testimonial test)
        {
            if (test.Comments != null && test.Comments != "")
            {
                if (test.Image != null)
                {
                    if (!test.Image.Contains("Resources/Testimonial"))
                    {
                        test.Image = GernalFunction.ImageCroping(test.Image, "/Resources/Testimonial/", false);
                    }
                    else
                    {
                        test.Image = Path.GetFileName(test.Image);
                    }
                }
                else
                {
                    test.Image = "NoImage.jpg";
                }
                test.Date = DateTime.Now;
                if (Convert.ToInt32(Request.QueryString["ID"]) != 0 && Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
                {
                    test.ID = Convert.ToInt32(Request.QueryString["ID"]);
                    new TestimonialModel().Update(test);
                    TempData["AlertTask"] = "Record Successfully Update";
                }
                else
                {
                    new TestimonialModel().Save(test);
                    TempData["AlertTask"] = "Record Successfully Saved";
                }
                return RedirectToAction("ManageTestimonials");
            }
            else
            {
                TempData["AlertTask"] = "Please Enter the Description";
                return View();
            }
        }

        public ActionResult ManageTestimonials()
        {
            List<Testimonial> lst = new TestimonialModel().GetAllTestimonials();
            return View(lst);
        }

        public void Delete(int ID)
        {
            new TestimonialModel().Delete(ID);
            TempData["AlertTask"] = "Record Successfully Deleted";
            Response.Redirect("ManageTestimonials");
        }
    }
}