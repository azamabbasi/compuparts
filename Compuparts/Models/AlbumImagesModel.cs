﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class AlbumImagesModel
    {
        genericadminEntities db = new genericadminEntities();

        public void Save(AlbumImage imges)
        {
            try
            {
                db.AlbumImages.Add(imges);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(AlbumImage imges)
        {
            try
            {
                db.AlbumImages.Attach(imges);
                var Update = db.Entry(imges);
                Update.Property(x => x.Image).IsModified = true;
                Update.Property(x => x.AlbumID).IsModified = true;
                Update.Property(x => x.Description).IsModified = true;
                Update.Property(x => x.SortOrder).IsModified = true;

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<AlbumImage> GetAllAlbumImage()
        {
            return db.AlbumImages.OrderByDescending(x => x.ID).ToList();
        }

        public AlbumImage GetAlbumImageByID(int ID)
        {
            return db.AlbumImages.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.AlbumImages.Where(x => x.ID == ID).FirstOrDefault();
            db.AlbumImages.Remove(item);
            db.SaveChanges();

        }

        public void DeleteByAlbumID(int ID)
        {
            var itemDel = db.AlbumImages.Where(x => x.AlbumID == ID).ToList();
            foreach (var item in itemDel)
            {
                db.AlbumImages.Remove(item);
                db.SaveChanges();
            }
            

        }

        public List<AlbumImage> GetAlbumImageByAlbumID(int AlbumID)
        {
            return db.AlbumImages.Where(x=>x.AlbumID == AlbumID).ToList();
        }
    }
}