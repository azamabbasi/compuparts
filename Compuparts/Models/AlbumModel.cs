﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class AlbumModel
    {
        genericadminEntities db = new genericadminEntities();
        public void Save(Album albm)
        {
            try
            {
                db.Albums.Add(albm);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(Album albm)
        {
            try
            {
                db.Albums.Attach(albm);
                var Update = db.Entry(albm);
                Update.Property(x => x.Name).IsModified = true;
                Update.Property(x => x.Url).IsModified = true;
                Update.Property(x => x.Description).IsModified = true;
                Update.Property(x => x.SortOrder).IsModified = true;

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<Album> GetAllAlbum()
        {
            return db.Albums.OrderByDescending(x => x.ID).ToList();
        }

        public Album GetAlbumByID(int ID)
        {
            return db.Albums.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Albums.Where(x => x.ID == ID).FirstOrDefault();
            db.Albums.Remove(item);
            db.SaveChanges();

        }
    }
}