﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class BillingInfoModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();
        public void Save(BillingInfo info)
        {
            db.BillingInfoes.Add(info);
            db.SaveChanges();
        }

        public BillingInfo GetByID(int ID)
        {
            return db.BillingInfoes.Where(x => x.ID == ID).FirstOrDefault();
        }

        public List<BillingInfo> GetByAll()
        {
            return db.BillingInfoes.ToList();
        }
    }
}