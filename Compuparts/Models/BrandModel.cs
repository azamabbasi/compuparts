﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class BrandModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Brand brnd)
        {
            db.Brands.Add(brnd);
            db.SaveChanges();
        }

        public void Update(Brand brnd)
        {
            db.Brands.Attach(brnd);
            var Update = db.Entry(brnd);
            Update.Property(x => x.Image).IsModified = true;
            Update.Property(x => x.Name).IsModified = true;
            db.SaveChanges();
        }

        public List<Brand> GetAll()
        {
            return db.Brands.OrderBy(x=>x.Name).ToList();
        }

        public Brand GetBrandByID(int ID)
        {
            return db.Brands.Where(x=>x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Brands.Where(x => x.ID == ID).FirstOrDefault();
            db.Brands.Remove(item);
            db.SaveChanges();

        }

        public List<Brand> GetRandomBrands()
        {
            return db.Brands.OrderBy(x => Guid.NewGuid()).Take(10).ToList();
        }

        
    }
}