﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class CartLineModel
    {
        protected compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(CartLine cl)
        {
            db.CartLines.Add(cl);
            db.SaveChanges();
        }

        public List<CartLine> GetCartLineByCartID(int CartID)
        {
            return db.CartLines.Where(x => x.CartID == CartID).ToList();
        }

        public void Update(CartLine cl)
        {
            db.CartLines.Attach(cl);
            var Update = db.Entry(cl);
            Update.Property(x => x.Quantity).IsModified = true;
            Update.Property(x => x.TotalPrice).IsModified = true;
            db.SaveChanges();
        }

        public CartLine GetCartLineByCartIDAndProductID(int CartID, int ProductID)
        {
            return db.CartLines.Where(x => x.CartID == CartID && x.ProductID == ProductID).FirstOrDefault();
        }

        public void Remove(int CartID, int ProductID)
        {
            var crt = db.CartLines.Where(x => x.CartID == CartID && x.ProductID == ProductID).FirstOrDefault();
            if (crt != null)
            {
                db.CartLines.Remove(crt);
                db.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            var crt = db.CartLines.Where(x => x.ID == id).FirstOrDefault();
            db.CartLines.Remove(crt);
            db.SaveChanges();
        }
    }
}