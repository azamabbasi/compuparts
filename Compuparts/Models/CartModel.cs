﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class CartModel
    {
        protected compuparts_dbEntities db = new compuparts_dbEntities();

        public int Save(Cart crt)
        {
            db.Carts.Add(crt);
            db.SaveChanges();
            int ID = crt.ID;
            return ID;
        }

        public Cart GetCartByGUID(string GUID)
        {
            Guid checkGUID = new Guid(GUID);
            return db.Carts.Where(x => x.Guid == checkGUID).FirstOrDefault();   
        }

        public Cart GetCartByCartID(int CartID)
        {
            return db.Carts.Where(x => x.ID == CartID).FirstOrDefault();   
        }

        public List<Cart> GetAll()
        {
            return db.Carts.OrderByDescending(x=>x.ID).ToList();
        }

        public void UpdateCart(string GUID, string hdnFinalPrice, string PaymentVia)
        {
            Guid checkGUID = new Guid(GUID);
            Cart c =  db.Carts.Where(x => x.Guid == checkGUID).FirstOrDefault();
            c.TotalPrice = Convert.ToDecimal(hdnFinalPrice);
            c.PaymentMethod = PaymentVia;
            c.Date = DateTime.Now;

            db.Carts.Attach(c);
            var update = db.Entry(c);
            update.Property(x => x.TotalPrice).IsModified = true;
            update.Property(x => x.PaymentMethod).IsModified = true;
            update.Property(x => x.Date).IsModified = true;
            db.SaveChanges();
        }

        public void UpdateStatus(int ID,string Comments,string Status)
        {
            var user = new Cart()
            {
                ID = ID,
                Comments = Comments,
                OrderStatus = Status
            };
            using (var dbMember = new compuparts_dbEntities())
            {
                dbMember.Carts.Attach(user);
                dbMember.Entry(user).Property(x => x.OrderStatus).IsModified = true;
                dbMember.Entry(user).Property(x => x.Comments).IsModified = true;
                dbMember.SaveChanges();
                
            }
        }

        public void UpdateDateAndPrice(int ID, string Date, string Price)
        {
            var user = new Cart()
            {
                ID = ID,
                Date =Convert.ToDateTime(Date),
                TotalPrice= Convert.ToDecimal(Price)
            };
            using (var dbMember = new compuparts_dbEntities())
            {
                dbMember.Carts.Attach(user);
                dbMember.Entry(user).Property(x => x.Date).IsModified = true;
                dbMember.Entry(user).Property(x => x.TotalPrice).IsModified = true;
                dbMember.SaveChanges();

            }
        }


        public void OrderConfirm(int ID)
        {
            var user = new Cart()
            {
                ID = ID,
                PaymentStatus= true
            };
            using (var dbMember = new compuparts_dbEntities())
            {
                dbMember.Carts.Attach(user);
                dbMember.Entry(user).Property(x => x.PaymentStatus).IsModified = true;
                dbMember.SaveChanges();

            }
        }

        public void ReadStatus(int ID)
        {
            var user = new Cart()
            {
                ID = ID,
                ReadStatus = true
            };
            using (var dbMember = new compuparts_dbEntities())
            {
                dbMember.Carts.Attach(user);
                dbMember.Entry(user).Property(x => x.ReadStatus).IsModified = true;
                dbMember.SaveChanges();
            }
        }
    }
}