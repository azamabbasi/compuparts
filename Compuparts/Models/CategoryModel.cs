﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class CategoryModel
    {
        protected compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Category cate)
        {
            db.Categories.Add(cate);
            db.SaveChanges();
        }

        public void Update(Category cate)
        {
            db.Categories.Attach(cate);
            var update = db.Entry(cate);
            update.Property(x => x.CategoryName).IsModified = true;
            update.Property(x => x.SortOrder).IsModified = true;
            update.Property(x => x.ParentCategoryID).IsModified = true;
            db.SaveChanges();
        }

        public List<Category> GetAll()
        {
            //List<Category> lst = new List<Category>();
            //lst = db.Categories.OrderBy(x => x.SortOrder).ToList();
            //foreach (var item in lst.Where(x => x.ParentCategoryID == null))
            //{
            //    lst.Where(x => x.ParentCategoryID == item.ID).ToList().ForEach(a =>
            //    {
            //        a.CategoryName = "==> " + a.CategoryName;
            //        lst.Where(s => s.ParentCategoryID == a.ID).ToList().ForEach(q =>
            //        {
            //            q.CategoryName = "====> " + q.CategoryName;
            //            lst.Where(d => d.ParentCategoryID == q.ID).ToList().ForEach(w =>
            //            {
            //                w.CategoryName = "======> " + w.CategoryName;
            //            });
            //        });
            //    });
            //}
            return db.Categories.OrderBy(x => x.SortOrder).ToList();
        }

        public List<Category> GetAllSubCategory()
        {
            return db.Categories.Where(x => x.ParentCategoryID != null).OrderBy(x => x.SortOrder).ToList();
        }

        public List<Category> GetAllMainCategory()
        {
            return db.Categories.Where(x => x.ParentCategoryID == null).OrderBy(x => x.SortOrder).ToList();
        }
        public List<Category> GetAllSubCategoryByParentID(int ParentID)
        {
            return db.Categories.Where(x => x.ParentCategoryID == ParentID).OrderBy(x => x.SortOrder).ToList();
        }


        public Category GetbyID(int ID)
        {
            return db.Categories.OrderByDescending(x => x.ID).Where(x => x.ID == ID).FirstOrDefault();
        }

        public Category GetbyName(string Name)
        {
            return db.Categories.OrderByDescending(x => x.ID).Where(x => x.CategoryName == Name).FirstOrDefault();
        }

        public void Delete(int CategoryID)
        {
            var item = db.Categories.Where(x => x.ID == CategoryID).FirstOrDefault();
            db.Categories.Remove(item);
            db.SaveChanges();

        }

        public bool IsExist(int ID)
        {
            var item = db.Categories.Where(x => x.ParentCategoryID == ID).ToList();
            bool Exist = true;
            if (item.Count == 0)
            {
                Exist = false;
            }
            return Exist;
        }
        public List<Category> GetAllInSameLine()
        {

            string sub1 = "";
            string sub2 = "";
            string sub3 = "";
            List<Category> lst = new List<Category>();
            lst = db.Categories.OrderBy(x => x.SortOrder).ToList();
            foreach (var item in lst.Where(x => x.ParentCategoryID == null))
            {
                lst.Where(x => x.ParentCategoryID == item.ID).ToList().ForEach(a =>
                {
                    sub1 = a.CategoryName;
                    a.CategoryName = item.CategoryName + " ==> " + a.CategoryName;
                    lst.Where(s => s.ParentCategoryID == a.ID).ToList().ForEach(q =>
                    {
                        sub2 = q.CategoryName;
                        q.CategoryName = item.CategoryName + " ==> " + sub1 + " ==> " + q.CategoryName;
                        lst.Where(d => d.ParentCategoryID == q.ID).ToList().ForEach(w =>
                        {
                            sub3 = w.CategoryName;
                            w.CategoryName = item.CategoryName + " ==> " + sub1 + " ==> " + sub2 + " ==> " + w.CategoryName;
                        });
                    });
                }
                );
            }
            return lst;
        }
    }
}