﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ColourModel
    {
        protected compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Colour col)
        {
            db.Colours.Add(col);
            db.SaveChanges();
        }

        public void Update(Colour col)
        {
            db.Colours.Attach(col);
            var update = db.Entry(col);
            update.Property(x => x.Name).IsModified = true;
            db.SaveChanges();
        }

        public List<Colour> GetAll()
        {
            return db.Colours.OrderByDescending(x => x.ID).ToList();
        }

        public Colour GetbyID(int ID)
        {
            return db.Colours.OrderByDescending(x => x.ID).Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Colours.Where(x => x.ID == ID).FirstOrDefault();
            db.Colours.Remove(item);
            db.SaveChanges();
        }
    }
}