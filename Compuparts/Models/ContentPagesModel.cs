﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ContentPagesModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ContentPage page)
        {
            try
            {
                db.ContentPages.Add(page);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(ContentPage page)
        {
            try
            {
                db.ContentPages.Attach(page);
                var Update = db.Entry(page);
                Update.Property(x => x.Heading).IsModified = true;
                Update.Property(x => x.URL).IsModified = true;
                Update.Property(x => x.Description).IsModified = true;
                Update.Property(x => x.Date).IsModified = true;
                Update.Property(x => x.UserID).IsModified = true;
                Update.Property(x => x.MetaTitle).IsModified = true;
                Update.Property(x => x.MetaKeyword).IsModified = true;
                Update.Property(x => x.MetaDescription).IsModified = true;
                Update.Property(x => x.PageName).IsModified = true;

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<ContentPage> GetAllContentUnit()
        {
            return db.ContentPages.OrderByDescending(x => x.ID).ToList();
        }

        public ContentPage GetContentPageByID(int ID)
        {
            return db.ContentPages.Where(x => x.ID == ID).FirstOrDefault();
        }
        public ContentPage GetContentPageByUrl(string Url)
        {
            return db.ContentPages.Where(x => x.URL == Url).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.ContentPages.Where(x => x.ID == ID).FirstOrDefault();
            db.ContentPages.Remove(item);
            db.SaveChanges();

        }
    }
}