﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ContentUnitModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ContentUnit unit)
        {
            try
            {
                db.ContentUnits.Add(unit);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(ContentUnit unit)
        {
            try
            {
                db.ContentUnits.Attach(unit);
                var Update = db.Entry(unit);
                Update.Property(x => x.UnitCode).IsModified = true;
                Update.Property(x => x.UnitName).IsModified = true;
                Update.Property(x => x.Description).IsModified = true;
                Update.Property(x => x.Date).IsModified = true;
                Update.Property(x => x.UserID).IsModified = true;


                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<ContentUnit> GetAllContentUnit()
        {
            return db.ContentUnits.OrderByDescending(x=>x.ID).ToList();
        }

        public ContentUnit GetContentUnitByID(int ID)
        {
            return db.ContentUnits.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.ContentUnits.Where(x => x.ID == ID).FirstOrDefault();
            db.ContentUnits.Remove(item);
            db.SaveChanges();

        }
    }
}