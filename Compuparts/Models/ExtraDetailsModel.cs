﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ExtraDetailsModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ExtraDetail EtrDetail)
        {
            db.ExtraDetails.Add(EtrDetail);
            db.SaveChanges();
        }

        public void Update(ExtraDetail EtrDetail)
        {
            db.ExtraDetails.Attach(EtrDetail);
            var Update = db.Entry(EtrDetail);
            Update.Property(x => x.Detail).IsModified = true;
            Update.Property(x => x.Name).IsModified = true;
            db.SaveChanges();
        }

        public List<ExtraDetail> GetAll()
        {
            return db.ExtraDetails.OrderBy(x => x.Name).ToList();
        }

        public ExtraDetail GetExtraDetailsByID(int ID)
        {
            return db.ExtraDetails.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.ExtraDetails.Where(x => x.ID == ID).FirstOrDefault();
            db.ExtraDetails.Remove(item);
            db.SaveChanges();

        }
    }
}