﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ExtraImagesModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ExtraImage Img)
        {
            try
            {
                db.ExtraImages.Add(Img);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(ExtraImage Img)
        {
            try
            {
                db.ExtraImages.Attach(Img);
                var Update = db.Entry(Img);
                Update.Property(x => x.Image).IsModified = true;
                Update.Property(x => x.Url).IsModified = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<ExtraImage> GetAllExtraImage()
        {
            return db.ExtraImages.OrderByDescending(x => x.ID).ToList();
        }

        public ExtraImage GetExtraImageByID(int ID)
        {
            return db.ExtraImages.Where(x => x.ID == ID).FirstOrDefault();
        }
    }
}