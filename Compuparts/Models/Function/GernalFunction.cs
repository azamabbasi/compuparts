﻿using Microsoft.Ajax.Utilities;
using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.Models.Function
{
    public class GernalFunction
    {
        public static string ImageCroping(string crop, string Filepath, bool thumbnail)
        {
            if (!String.IsNullOrEmpty(crop))
            {
                var fileName = "";
                var imageStr64 = crop;
                var imgCode = imageStr64.Split(',');

                var bytes = Convert.FromBase64String(imgCode[1]);

                using (var stream = new MemoryStream(bytes))
                {
                    using (var img = System.Drawing.Image.FromStream(stream))
                    {
                        var filename = "";
                        var fileext = "";
                        if (imgCode.Contains("png"))
                        {
                            filename = "Image" + DateTime.UtcNow;
                            fileext = ".png";
                        }
                        else
                        {
                            filename = "Image" + DateTime.UtcNow;
                            fileext = ".jpg";
                        }

                        string fileNameNew = filename;
                        fileNameNew = RemoveSpecialCharacters(fileNameNew);


                        var path = HttpContext.Current.Server.MapPath(Filepath);

                        var ext = Path.GetExtension(filename);

                        var i = 0;
                        while (File.Exists(path + fileNameNew + fileext))
                        {
                            i++;
                            fileNameNew = Path.GetFileNameWithoutExtension(filename);
                            fileNameNew += i;
                        }


                        fileName = fileNameNew + fileext;
                        var filePath = HttpContext.Current.Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath + Filepath + fileName);
                        File.WriteAllBytes(filePath, bytes);
                        if (thumbnail == true)
                        {
                            string ThumbImgName = fileName.Replace("Image", "Thumb");
                            Resize_Image_Thumb(filePath, fileName, 63, 51, ThumbImgName);
                        }
                    }

                }

                return fileName;
            }
            else
            {
                return null;
            }
        }

        public static void Resize_Image_Thumb(string filePath, string srcImgName, int newWidth, int newHeight, string ThumbImgName)
        {
            //Get Destinatino Path of image
            string newImgName = null;
            string newImagePath = null;
            string imgName = null;
            string imgExt = null;

            string[] NameExtArray = srcImgName.Split(new char[] { '.' });
            imgName = NameExtArray[0];
            imgExt = NameExtArray[1];
            newImgName = ThumbImgName;
            newImagePath = filePath.Replace(srcImgName, ThumbImgName);

            Image imgPhoto = Image.FromFile(filePath);

            int sourceWidth = 0;
            int sourceHeight = 0;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            double nPercent = 0;
            double nPercentW = 0;
            double nPercentH = 0;
            sourceWidth = imgPhoto.Width;
            sourceHeight = imgPhoto.Height;
            sourceX = 0;
            sourceY = 0;
            destX = 0;
            destY = 0;

            nPercent = 0;
            nPercentW = 0;
            nPercentH = 0;

            nPercentW = (Convert.ToDouble(newWidth) / Convert.ToDouble(sourceWidth));
            nPercentH = (Convert.ToDouble(newHeight) / Convert.ToDouble(sourceHeight));

            if ((nPercentH < nPercentW))
            {
                nPercent = nPercentW;
                destY = Convert.ToInt32(((newHeight - (sourceHeight * nPercent)) / 2));
            }
            else
            {
                nPercent = nPercentH;
                destX = Convert.ToInt32(((newWidth - (sourceWidth * nPercent)) / 2));
            }

            int destWidth = 0;
            int destHeight = 0;
            destWidth = Convert.ToInt32((sourceWidth * nPercent));
            destHeight = Convert.ToInt32((sourceHeight * nPercent));


            Bitmap bmPhoto = null;
            bmPhoto = new Bitmap(newWidth, newHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
            Graphics grPhoto = null;
            grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.DrawImage(imgPhoto, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);

            grPhoto.Dispose();
            bmPhoto.Save(newImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            bmPhoto.Dispose();
            imgPhoto.Dispose();
        }

        public void SetCookie(User user)
        {
            HttpCookie AdminCookie = new HttpCookie("AdminCookies");
            AdminCookie.Values["UserName"] = user.UserName.ToString();
            AdminCookie.Values["Password"] = user.Password.ToString();
            AdminCookie.Values["ID"] = user.ID.ToString();
            HttpContext.Current.Response.SetCookie(AdminCookie);
        }

        public void CheckAdminLogin()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["AdminCookies"];
            if (cookie == null || String.IsNullOrEmpty(Convert.ToString(cookie)))
            {
                HttpContext.Current.Response.Redirect("/User/Login");
            }
            else if (cookie.Values["Password"] == "-")
            {
                string url = "/User/LockScreen?ID=" + cookie.Values["ID"].ToString();
                HttpContext.Current.Response.Redirect(url);
                
            }

        }

        public SelectList CategoryList()
        {
            var CategoryList = new SelectList(new CategoryModel().GetAll().Select(x => new { x.ID, x.CategoryName }).OrderBy(x => x.CategoryName).ToList(), "ID", "CategoryName");
            return CategoryList;
        }

        public SelectList BrandList()
        {
            var BrandLst = new SelectList(new BrandModel().GetAll().Select(x => new { x.ID, x.Name}).OrderBy(x => x.Name).ToList(), "ID", "Name");
            return BrandLst;
        }

        public SelectList ExtraDetailList()
        {
            var ExtraDetailLst = new SelectList(new ExtraDetailsModel().GetAll().Select(x => new { x.ID, x.Name }).OrderBy(x => x.Name).ToList(), "ID", "Name");
            return ExtraDetailLst;
        }

        //public SelectList AlbumList()
        //{
        //    var Album = new SelectList(new AlbumModel().GetAllAlbum().Select(x => new { x.ID, x.Name }).OrderBy(x => x.Name).ToList(), "ID", "Name");
        //    return Album;
        //}

        public void EmailHostDetail(string toEmail, string fromEmail, string subject, string bodyHtml, string fileName)
        {
            //File Path
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "Resources\\Files\\";

            //email sending code will appear here.
            MailMessage message = new MailMessage();
            SmtpClient client = new SmtpClient();

            message.To.Add(new MailAddress(toEmail));

            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = bodyHtml;

            if (!String.IsNullOrEmpty(fileName))
            {

                //DOING ATTACHMENT:
                Attachment data =
                    new Attachment(filePath + fileName,
                        MediaTypeNames.Application.Octet);
                // Add time stamp information for the file.
                ContentDisposition disposition = data.ContentDisposition;
                // Add the file attachment to this e-mail message.
                message.Attachments.Add(data);
            }


            string hostname = "smtp.sendgrid.net";// "smtp.gmail.com";
            client.Host = hostname;
            client.Port = 587;
            client.EnableSsl = true;
            string username = "alipk3";
            string password = "Emailsend94";
            message.From = new MailAddress(fromEmail, "NBK");
            var basicCredentials = new System.Net.NetworkCredential(username, password);
            client.Credentials = basicCredentials;
            client.Send(message);
        }
        public static string GetHmtlContentsFromDirectory(string FileName)
        {
            string Result = "";
            string FilePath = HttpContext.Current.Server.MapPath(FileName);
            System.IO.StreamReader MyFile = new System.IO.StreamReader(FilePath);
            Result = MyFile.ReadToEnd();
            MyFile.Close();
            MyFile.Dispose();
            return Result;
        }

        public static string RemoveSpecialCharacters(string input)
        {
            Regex r = new Regex("(?:[^a-zA-Z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(input, String.Empty);
        }

        public enum OrderStatus
        {
            Pending,
            Inprocess,
            Shipped,
            Cancelled,
        };

        private List<Setting> listOfSetting;
        public List<Setting> ListOfSetting
        {
            get
            {
                if (listOfSetting != null)
                    return listOfSetting;

                return listOfSetting = new SettingsModel().GetAllSettings().ToList();
            }

        }
    }
}