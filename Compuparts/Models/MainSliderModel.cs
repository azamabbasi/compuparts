﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class MainSliderModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(MainSlider slider)
        {
            try
            {
                db.MainSliders.Add(slider);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(MainSlider slider)
        {
            try
            {
                db.MainSliders.Attach(slider);
                var Update = db.Entry(slider);
                Update.Property(x => x.Title).IsModified = true;
                Update.Property(x => x.Description).IsModified = true;
                Update.Property(x => x.Image).IsModified = true;
                Update.Property(x => x.SortOrder).IsModified = true;
                Update.Property(x => x.URL).IsModified = true;

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<MainSlider> GetAllMainSlider()
        {
            return db.MainSliders.OrderByDescending(x => x.ID).ToList();
        }

        public MainSlider GetMainSliderByID(int ID)
        {
            return db.MainSliders.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.MainSliders.Where(x => x.ID == ID).FirstOrDefault();
            db.MainSliders.Remove(item);
            db.SaveChanges();

        }
    }
}