﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ProductImagesModel
    {

        protected compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ProductImage img)
        {
            db.ProductImages.Add(img);
            db.SaveChanges();
        }

        public void DeleteByProductID(int ID)
        {
            var itemDel = db.ProductImages.Where(x => x.ProductID == ID).ToList();
            foreach (var item in itemDel)
            {
                db.ProductImages.Remove(item);
                db.SaveChanges();
            }
        }

        public List<ProductImage> GetAllByProductID(int ID)
        {
            return db.ProductImages.Where(x => x.ProductID == ID).ToList();
        }

    }
}