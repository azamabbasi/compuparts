﻿using Compuparts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ProductModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public int Save(Product pd)
        {
            db.Products.Add(pd);
            db.SaveChanges();
            return pd.ID;
        }

        public void Update(Product pd)
        {
            db.Products.Attach(pd);
            var Update = db.Entry(pd);
            Update.Property(x => x.Name).IsModified = true;
            Update.Property(x => x.CategoryID).IsModified = true;
            Update.Property(x => x.Description).IsModified = true;
            Update.Property(x => x.Keyword).IsModified = true;
            Update.Property(x => x.Url).IsModified = true;
            Update.Property(x => x.PriceWithoutVAT).IsModified = true;
            Update.Property(x => x.Featured).IsModified = true;
            Update.Property(x => x.PriceWithVAT).IsModified = true;
            Update.Property(x => x.Sale).IsModified = true;
            Update.Property(x => x.PriceWithVAT).IsModified = true;
            Update.Property(x => x.EAN).IsModified = true;
            Update.Property(x => x.UAN).IsModified = true;
            Update.Property(x => x.Colour).IsModified = true;
            Update.Property(x => x.Quantity).IsModified = true;
            Update.Property(x => x.Subtitle).IsModified = true;
            Update.Property(x => x.Used).IsModified = true;
            Update.Property(x => x.Refurbished).IsModified = true;
            Update.Property(x => x.OriginalUsed).IsModified = true;
            Update.Property(x => x.Offer).IsModified = true;
            Update.Property(x => x.Damaged).IsModified = true;
            Update.Property(x => x.New).IsModified = true;
            Update.Property(x => x.ExtraDetailID).IsModified = true;
            Update.Property(x => x.SentInternational).IsModified = true;
            db.SaveChanges();
        }

        public List<Product> GetAll()
        {
            return db.Products.Where(x => x.Is_Deleted == false).OrderByDescending(x => x.ID).ToList();
        }

        public List<Product> GetTop4PhoneParts()
        {
            List<Product> lst = new List<Product>();
            string Query = " select Top(4) * from Products  where Featured = 1 and Is_Deleted = 0 and CategoryID in (select ID from dbo.Categories where ParentCategoryID in ( select ID from dbo.Categories where ParentCategoryID = 1)) order by ID desc ";
            lst = db.Products.SqlQuery(Query).ToList();
            return lst;
        }

        public List<Product> GetTop4TabletParts()
        {
            List<Product> lst = new List<Product>();
            string Query = " select Top(4) * from Products where Featured = 1 and Is_Deleted = 0 and CategoryID in (select ID from dbo.Categories where ParentCategoryID in ( select ID from dbo.Categories where ParentCategoryID = 70)) order by ID desc ";
            lst = db.Products.SqlQuery(Query).ToList();
            return lst;
        }

        public List<Product> GetTop4LaptopParts()
        {
            List<Product> lst = new List<Product>();
            string Query = " select Top(4) * from Products  where Featured = 1 and Is_Deleted = 0 and  CategoryID in (select ID from dbo.Categories where ParentCategoryID in ( select ID from dbo.Categories where ParentCategoryID = 145)) order by ID desc ";
            lst = db.Products.SqlQuery(Query).ToList();
            return lst;
        }

        public List<Product> GetTop4OtherElectronicsParts()
        {
            List<Product> lst = new List<Product>();
            string Query = " select Top(4) * from Products where  Featured = 1 and Is_Deleted = 0 and  CategoryID in (select ID from dbo.Categories where ParentCategoryID in ( select ID from dbo.Categories where ParentCategoryID = 17)) order by ID desc ";
            lst = db.Products.SqlQuery(Query).ToList();
            return lst;
        }

        public Product GetProductByID(int ID)
        {
            return db.Products.Where(x => x.ID == ID && x.Is_Deleted == false).FirstOrDefault();
        }

        public List<Product> GetAllProductsByCategoryID(int CategoryID)
        {
            return db.Products.Where(x => x.CategoryID == CategoryID && x.Is_Deleted == false).ToList();
        }

        public bool IsExist(int ID)
        {
            var item = db.Products.Where(x => x.CategoryID == ID && x.Is_Deleted == false).ToList();
            bool Exist = true;
            if (item.Count == 0)
            {
                Exist = false;
            }
            return Exist;
        }

        public string Filters(string OrderBy)
        {
            string HTML = "";
            List<Product> MainList = HttpContext.Current.Session["ProductSearchResult"] as List<Product>;
            if (MainList.Count() > 0 && MainList != null)
            {
                if (OrderBy == "ascending")
                {
                    MainList = MainList.OrderBy(x => x.PriceWithVAT).ToList();
                }
                else
                {
                    MainList = MainList.OrderByDescending(x => x.PriceWithVAT).ToList();
                }
                HttpContext.Current.Session.Remove("ProductSearchResult");
                HttpContext.Current.Session.Add("ProductSearchResult", MainList);
            }


            foreach (var item in MainList)
            {
                HTML += "<li><div class='product-box'><div class='product-image'><span><a href='/Product/ProductDetail?ID=" + item.ID + "'><img src='/Resources/ProductImages/" + item.ProductImages.FirstOrDefault().Images + "' style='height:249px; width:249px;' class='img-responsive' alt='" + item.Name + "' />" +
                                                      "  </a></span></div><div class='product-details'><div class='product-name'><a href='/Product/ProductDetail?ID="+item.ID+"' title='" + item.Name + "'>" + item.Name + "</a></div><div class='price-group'>"+
                                                     "<div class='col-md-6 col-sm-12'> <div class='price ex-vat'><span><i>€</i>" + item.PriceWithoutVAT + "</span> - Ex VAT</div></div>"+
                                                      "<div class='col-md-6 col-sm-12'><div class='price inc-vat'><span><i>€</i>" + item.PriceWithVAT + "</span> - Inc VAT</div></div></div></div>" +
                                                        "<div class='cartQuantity' id=Quant_'" + item.ID + "'><div class='box'><div class='input-group'><span class='input-group-btn'><button type='button' class='btn btn-default btnRmvQty'>" +
                                                    "<span class='glyphicon glyphicon-minus'></span></button></span>" +
                                           " <input id='123' type='text' name='prodQty' class='form-control  proQtyNumber' value='0' min='1' max='" + item.Quantity + "' /><span class='input-group-btn'>" +
                                                "<button type='button' class='btn btn-default btnAddQty'><span class='glyphicon glyphicon-plus'></span></button></span></div></div></div>" +
                                                "<div class='button-group'>" +
                                     "<button title='Add To Cart' class='add-to-cart-button' onclick='Exist(" + item.ID + "," + item.PriceWithoutVAT + ")'><span class='fa fa-shopping-cart'></span>Add to cart</button>" +
                                    "<a href='/Product/ProductDetail?ID=" + item.ID + "' title='view details' class='product-details-button'>View Details</a></div></div></li>";

            }

            return HTML;

        }

        public List<Product> GetRandomProduct()
        {
            return db.Products.Where(x=>x.Is_Deleted== false).OrderBy(x => Guid.NewGuid()).Take(8).ToList();
        }

        public int Quantitychecker(Quantitycheck obj)
        {
            int Qunatity = 0;
            List<ProductQuantity> lst = new List<ProductQuantity>();
            if (obj.ProductID > 0)
            {
                if (obj.Size > 0 && obj.Colour > 0)
                {
                    lst = db.ProductQuantities.Where(x => x.ProductID == obj.ProductID && x.SizeID == obj.Size && x.ColourID == obj.Colour && x.Product.Is_Deleted == false).ToList();
                }
                else if (obj.Size > 0)
                {
                    lst = db.ProductQuantities.Where(x => x.ProductID == obj.ProductID && x.SizeID == obj.Size && x.Product.Is_Deleted == false).ToList();
                }
                else if (obj.Colour> 0 && obj.Colour > 0)
                {
                    lst = db.ProductQuantities.Where(x => x.ProductID == obj.ProductID && x.ColourID == obj.Colour && x.Product.Is_Deleted == false).ToList();
                }

                foreach (var item in lst)
                {
                    Qunatity = Qunatity + Convert.ToInt32(item.Quantity);
                }

            }
            return Qunatity;

        }

        public List<Product> GetAllProductsKeyword(string keyword)
        {
            string query2 = "";
            List<string> stringlst = new List<string>();

            if (!String.IsNullOrEmpty(keyword))
            {
                stringlst = keyword.Split(' ').ToList();
                int a = 1;
                foreach (var item in stringlst)
                {
                    if (a == 1)
                    {
                        query2 += "and (' ' + RTRIM(Keyword) + ' ') LIKE '%" + item + "%' ";
                        a++;
                    }
                    else
                    {
                        query2 += "or (' ' + RTRIM(Keyword) + ' ') LIKE '%" + item + "%' ";
                    }

                }

            }

            string Query = " select top (10)* from ( select  ROW_NUMBER() over(order by ID desc)as RowNumber, COUNT(*) OVER() AS TotalCount, * from product p where 1=1 " + query2 + " ) a where a.RowNumber >0 * 10 ";

            return db.Products.SqlQuery(Query).ToList().Where(x => x.Is_Deleted == false).ToList();
        }

        public void UpdateURL(int ID, string URL)
        {
            var user = new Product()
            {
                ID = ID,
                Url = URL
            };
            using (var dbMember = new compuparts_dbEntities())
            {
                dbMember.Products.Attach(user);
                dbMember.Entry(user).Property(x => x.Url).IsModified = true;
                dbMember.SaveChanges();
            }
        }


    }
}