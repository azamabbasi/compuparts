﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ProductQuantityModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ProductQuantity pdQty)
        {
            db.ProductQuantities.Add(pdQty);
            db.SaveChanges();
        }

        public void Update(ProductQuantity pdQty)
        {
            db.ProductQuantities.Attach(pdQty);
            var Update = db.Entry(pdQty);
            Update.Property(x => x.ProductID).IsModified = true;
            Update.Property(x => x.ColourID).IsModified = true;
            Update.Property(x => x.SizeID).IsModified = true;
            Update.Property(x => x.Quantity).IsModified = true;
        }

        public List<ProductQuantity> GetProductQuantityByProductID(int ID)
        {
            return db.ProductQuantities.Where(x => x.ProductID == ID).ToList();
        }

        public List<ProductQuantity> GetAll()
        {
            return db.ProductQuantities.GroupBy(p => p.ProductID).Select(g => g.FirstOrDefault()).ToList();
        }

        public bool IsExistColour(int ID)
        {
            var item = db.ProductQuantities.Where(x => x.ColourID == ID).ToList();
            bool Exist = true;
            if (item.Count == 0)
            {
                Exist = false;
            }
            return Exist;
        }
        public bool IsExistSize(int ID)
        {
            var item = db.ProductQuantities.Where(x => x.SizeID == ID).ToList();
            bool Exist = true;
            if (item.Count == 0)
            {
                Exist = false;
            }
            return Exist;
        }


        
    }
}