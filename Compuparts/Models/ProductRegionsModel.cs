﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class ProductRegionsModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(ProductRegion pr)
        {
            db.ProductRegions.Add(pr);
            db.SaveChanges();
        }


        public void Update (ProductRegion pr)
        {
            db.ProductRegions.Attach(pr);
            db.Entry(pr).Property(x => x.ProductID).IsModified = true;
            db.Entry(pr).Property(x => x.RegionID).IsModified = true;
            db.SaveChanges();
        }

        public List<ProductRegion> GetByProductID(int ProductID)
        {
            return db.ProductRegions.Where(x => x.ProductID == ProductID).ToList();
        }

        public void DeleteProductRegionByProductID(int ID)
        {
            var itemDel = db.ProductRegions.Where(x => x.ProductID == ID).ToList();
            foreach (var item in itemDel)
            {
                db.ProductRegions.Remove(item);
                db.SaveChanges();
            }
        }

    }
}