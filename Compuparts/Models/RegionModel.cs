﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class RegionModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Region rg)
        {
            db.Regions.Add(rg);
            db.SaveChanges();
        }

        public void Update(Region rg)
        {
            db.Regions.Attach(rg);
            db.Entry(rg).Property(x=>x.Name).IsModified =  true;
            db.SaveChanges();
        }

        public List<Region> GetAll()
        {
            return db.Regions.ToList();
        }

        public Region GetByID(int ID)
        {
            return db.Regions.Where(x=>x.ID ==  ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Regions.Where(x => x.ID == ID).FirstOrDefault();
            db.Regions.Remove(item);
            db.SaveChanges();

        }
    }
}