﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class SettingsModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Setting sting)
        {
            try
            {
                db.Settings.Add(sting);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(Setting sting)
        {
            try
            {
                db.Settings.Attach(sting);
                var Update = db.Entry(sting);
                Update.Property(x => x.Title).IsModified = true;
                Update.Property(x => x.Value).IsModified = true;


                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<Setting> GetAllSettings()
        {
            return db.Settings.OrderByDescending(x => x.ID).ToList();
        }

        public Setting GetSettingsByID(int ID)
        {
            return db.Settings.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Settings.Where(x => x.ID == ID).FirstOrDefault();
            db.Settings.Remove(item);
            db.SaveChanges();

        }
    }
}