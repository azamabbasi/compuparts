﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class SizeModel
    {

        protected compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Size sz)
        {
            db.Sizes.Add(sz);
            db.SaveChanges();
        }

        public void Update(Size sz)
        {
            db.Sizes.Attach(sz);
            var update = db.Entry(sz);
            update.Property(x => x.Name).IsModified = true;
            db.SaveChanges();
        }

        public List<Size> GetAll()
        {
            return db.Sizes.OrderByDescending(x => x.ID).ToList();
        }

        public Size GetbyID(int ID)
        {
            return db.Sizes.OrderByDescending(x => x.ID).Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Sizes.Where(x => x.ID == ID).FirstOrDefault();
            db.Sizes.Remove(item);
            db.SaveChanges();

        }
    }
}