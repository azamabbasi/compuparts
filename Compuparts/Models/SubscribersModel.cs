﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class SubscribersModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Subscriber sub)
        {
            try
            {
                db.Subscribers.Add(sub);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(Subscriber sub)
        {
            try
            {
                db.Subscribers.Attach(sub);
                var Update = db.Entry(sub);
                Update.Property(x => x.Email).IsModified = true;

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<Subscriber> GetAllSubscriber()
        {
            return db.Subscribers.OrderByDescending(x => x.ID).ToList();
        }

        public Subscriber GetSubscriberByID(int ID)
        {
            return db.Subscribers.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Subscribers.Where(x => x.ID == ID).FirstOrDefault();
            db.Subscribers.Remove(item);
            db.SaveChanges();

        }
    }
}