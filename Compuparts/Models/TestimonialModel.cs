﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.Models
{
    public class TestimonialModel
    {
        compuparts_dbEntities db = new compuparts_dbEntities();

        public void Save(Testimonial test)
        {
            try
            {
                db.Testimonials.Add(test);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public void Update(Testimonial test)
        {
            try
            {
                db.Testimonials.Attach(test);
                var Update = db.Entry(test);
                Update.Property(x => x.Name).IsModified = true;
                Update.Property(x => x.Designation).IsModified = true;
                Update.Property(x => x.Comments).IsModified = true;
                Update.Property(x => x.Date).IsModified = true;
                Update.Property(x => x.Image).IsModified = true;

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<Testimonial> GetAllTestimonials()
        {
            return db.Testimonials.OrderByDescending(x => x.ID).ToList();
        }

        public Testimonial GetTestimonialsByID(int ID)
        {
            return db.Testimonials.Where(x => x.ID == ID).FirstOrDefault();
        }

        public void Delete(int ID)
        {
            var item = db.Testimonials.Where(x => x.ID == ID).FirstOrDefault();
            db.Testimonials.Remove(item);
            db.SaveChanges();

        }
    }
}