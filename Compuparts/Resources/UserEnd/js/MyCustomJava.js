﻿var Exists = "no";
var TotalItems = 0;

function Exist(item, Price) {
    debugger;
    var q =parseInt( $('#Quant_' + item + ' input').val()) ;
    if (q > 0) {
        if ($('#' + item)[0]) {

            CheckExistance(item, q);
        }
        else {
            AddCart(item, Price, q)
        }
    }
    $('#Quant_' + item + ' input').val('0');
}

function AddCart(item, Price, q) {
    debugger;
    $("#MainLoader").show();

    var dataobj = {
        ProductID: item,
        ProductPrice: Price,
        Quantity: q,
    }

    $.ajax({

        type: "POST",

        url: '/AjaxCalling/AddCart',

        data: JSON.stringify({ crt: dataobj }),

        contentType: "application/json; charset=utf-8",

        dataType: "json",

        success: function (response) {

        },
        error: function (response) {
            debugger;
            var str = response.responseText;
            $(".top").append(str);
            var HTML = "";
            if (Exists == "no") {
                HTML += " <div class='subtotal'><span class='text'>Subtotal:</span><span class='value'>0.00</span> <span class='Currency'>€</span> </div> <a href='/product/ShoppingCart' class='btnCheckOut'>Checkout Now</a> <a href='/product/ShoppingCart' title='view cart or edit' class='btnCart'>View Cart</a>";
                $(".bottom").html('');
                $(".bottom").html(HTML);
            }
            Exists = "yes";
            var FullSubTotal = parseFloat($('.value').text());
            var qty = $('.qty_' + item).text();
            var prc = $('.prc_' + item).text();
            var subtotal = parseFloat(qty).toFixed(2) * parseFloat(prc).toFixed(2);
            FullSubTotal = FullSubTotal + subtotal;
            $('.value').text(parseFloat(FullSubTotal).toFixed(2));
            addToCart();
            TotalItems = $("#CartCount").text();
            TotalItems++;
            $("#CartCount").text(TotalItems);
            $("#MainLoader").hide();
        },
        failure: function (response) {
            $("#MainLoader").hide();
            alert("Service Failed");

        }

    });

}

function CheckExistance(item, quantity) {

    $("#MainLoader").show();
    var dataobj = {
        ProductID: item,
        Quantity: quantity,
    }

    $.ajax({

        type: "POST",

        url: '/AjaxCalling/UpdateCart',

        data: JSON.stringify({ crt: dataobj }),

        contentType: "application/json; charset=utf-8",

        dataType: "json",

        success: function (response) {
           
            return false;
        },
        error: function (response) {
            
            var UpFullSubTotal = parseFloat($('.value').text());
            var str = response.responseText;
            var arr = str.split('-');
            var qty = arr[0];
            var productID = arr[1];
            var preQty = $('.qty_' + productID).text();
            preQty = parseInt(preQty) + parseInt(qty);
            $('.qty_' + productID).text(preQty);

            var prc = $('.prc_' + item).text();
            var subtotal = parseFloat(qty).toFixed(2) * parseFloat(prc).toFixed(2);
            UpFullSubTotal = UpFullSubTotal + subtotal;
            $('.value').text(parseFloat(UpFullSubTotal).toFixed(2));

            addToCart();
            $("#MainLoader").hide();

            return true;
        },
        failure: function (response) {
            $("#MainLoader").hide();
            alert("Service Failed");
            return false;
        }

    });

}

function Remove(item) {

    if ($('#' + item)[0]) {
        $("#MainLoader").show();
        var qty = $('.qty_' + item).text();
        var prc = $('.prc_' + item).text();
        var subtotal = parseFloat(qty).toFixed(2) * parseFloat(prc).toFixed(2);
        var FullSubTotal = parseFloat($('.value').text());
        FullSubTotal = FullSubTotal - subtotal;
        $('.value').text(FullSubTotal);
        $('#' + item).remove();
        TotalItems--;
        $("#CartCount").text(TotalItems);
        if (FullSubTotal == 0) {
            $(".bottom").html('');
            $(".bottom").html("   <a href='#' title='view cart or edit' class='btnCart'>You have no items in your shopping cart</a>");
        }
        addToCart();
        $.ajax({

            type: "POST",

            url: '/AjaxCalling/RemoveCartLine',

            data: "{'ID':'" + item + "'}",

            contentType: "application/json; charset=utf-8",

            dataType: "json",

            success: function (response) {

            },
            error: function (response) {


            },
            failure: function (response) {

            }

        });
        $("#MainLoader").hide();
    }

    else {

    }
}






