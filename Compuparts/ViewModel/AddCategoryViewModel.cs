﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.ViewModel
{
    public class AddCategoryViewModel
    {
        int ID = Convert.ToInt32(HttpContext.Current.Request.QueryString["ID"]);

        public Category Category
        {
            get;
            set;
        }

        List<Category> categoryList;
        public List<Category> CategoryList
        {
            get
            {
                if (categoryList != null)
                {
                    return categoryList;
                }
                else
                {
                    return categoryList = new CategoryModel().GetAllInSameLine();
                }
            }
        }
    }
}