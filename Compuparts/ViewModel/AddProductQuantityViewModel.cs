﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.ViewModel
{
    public class AddProductQuantityViewModel
    {
        List<SelectListItem> product;
        public List<SelectListItem> Product
        {
            get
            {
                if (product != null)
                {
                    return product;
                }
                else
                {
                    return product = new ProductModel().GetAll().Select(x => new SelectListItem() { Text = x.Name, Value = x.ID.ToString() }).ToList();
                }
            }

        }

        List<SelectListItem> size;
        public List<SelectListItem> Size
        {
            get
            {
                if (size != null)
                {
                    return size;
                }
                else
                {
                    return size = new SizeModel().GetAll().Select(x => new SelectListItem() { Text = x.Name, Value = x.ID.ToString() }).ToList();
                }
            }

        }

        

        List<SelectListItem> colour;
        public List<SelectListItem> Colour
        {
            get
            {
                if (colour != null)
                {
                    return colour;
                }
                else
                {
                    return colour = new ColourModel().GetAll().Select(x => new SelectListItem() { Text = x.Name, Value = x.ID.ToString() }).ToList();
                }
            }

        }


        List<SelectListItem> brand;
        public List<SelectListItem> Brand
        {
            get
            {
                if (brand != null)
                {
                    return brand;
                }
                else
                {
                    return brand = new ColourModel().GetAll().Select(x => new SelectListItem() { Text = x.Name, Value = x.ID.ToString() }).ToList();
                }
            }

        }

        public ProductQuantity ProductQty { get; set; }

        public int ProductId
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Request.QueryString["ID"]);
            }
        }


        List<ProductQuantity> listProductquantity;
        public List<ProductQuantity> ListProductQuantity
        {
            get
            {
                if (listProductquantity != null)
                {
                    return listProductquantity;
                }
                else
                {
                    return listProductquantity = new ProductQuantityModel().GetProductQuantityByProductID(ProductId);
                }
            }

        }
        public List<ProductQuantity> ListOfProductQuantaties { get; set; }
    }
}