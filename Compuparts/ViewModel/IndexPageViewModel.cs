﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.ViewModel
{
    public class IndexPageViewModel
    {
        List<MainSlider> slider;
        public List<MainSlider> Slider
        {
            get
            {
                if(slider != null)
                {
                    return slider;
                }
                else
                {
                    return slider = new MainSliderModel().GetAllMainSlider();
                }
            }
        }

        List<ExtraImage> extraImages;
        public List<ExtraImage> ExtraImages
        {
            get
            {
                if (extraImages != null)
                {
                    return extraImages;
                }
                else
                {
                    return extraImages = new ExtraImagesModel().GetAllExtraImage();
                }
            }
        }

        List<Product> phoneParts;
        public List<Product> PhoneParts
        {
            get
            {
                if(phoneParts != null)
                {
                    return phoneParts;
                }
                else
                {
                    return phoneParts = new ProductModel().GetTop4PhoneParts();
                }
                
            }
        }

        List<Product> tabletParts;
        public List<Product> TabletParts
        {
            get
            {
                if (tabletParts != null)
                {
                    return tabletParts;
                }
                else
                {
                    return tabletParts = new ProductModel().GetTop4TabletParts();
                }

            }
        }

        List<Product> laptopParts;
        public List<Product> LaptopParts
        {
            get
            {
                if (laptopParts != null)
                {
                    return laptopParts;
                }
                else
                {
                    return laptopParts = new ProductModel().GetTop4LaptopParts();
                }

            }
        }

        List<Product> otherElectronicParts;
        public List<Product> OtherElectronicParts
        {
            get
            {
                if (otherElectronicParts != null)
                {
                    return otherElectronicParts;
                }
                else
                {
                    return otherElectronicParts = new ProductModel().GetTop4OtherElectronicsParts();
                }

            }
        }
    }
}