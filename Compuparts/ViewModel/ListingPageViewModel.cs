﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.ViewModel
{
    public class ListingPageViewModel
    {
        int QueryString = 0;
        public string SeacrhKeyword = "";
        public ListingPageViewModel(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                SeacrhKeyword = key;
            }
            else
            {
                int ID = Convert.ToInt32(HttpContext.Current.Request.RequestContext.RouteData.Values["ID"].ToString());
                QueryString = ID;
            }
        }
        
       
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }

        List<Product> productList;
        public List<Product> ProductList
        {
            get
            {
                if (productList != null)
                {
                    return productList;
                }
                else
                {
                    if (QueryString > 0)
                    {
                        productList = new ProductModel().GetAllProductsByCategoryID(QueryString).OrderBy(x => x.PriceWithVAT).ToList();
                        if (productList.Count > 0)
                        {
                            MetaTitle = productList.FirstOrDefault().MetaTitle;
                            MetaKeyword = productList.FirstOrDefault().MetaKeyword;
                            MetaDescription = productList.FirstOrDefault().MetaDescription;
                        }
                    }
                    else if (!string.IsNullOrEmpty(SeacrhKeyword))
                    {
                        MetaTitle = "Search for " + SeacrhKeyword;
                        MetaKeyword = "Search for " + SeacrhKeyword;
                        MetaDescription = "Search for " + SeacrhKeyword;
                        HttpContext.Current.Session.Add("ProductSearchFor", SeacrhKeyword);
                        productList = new ProductModel().GetAllProductsKeyword(SeacrhKeyword);
                    }
                    HttpContext.Current.Session.Add("ProductSearchResult", productList);
                    return productList;
                }
            }
        }
    }
}