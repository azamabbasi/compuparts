﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Compuparts.Models;

namespace Compuparts.ViewModel
{
    public class OrderViewModel
    {

       public int CartId
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Request.QueryString["ID"]);
            }
        }

        Cart cart;
        public Cart Cart
        {
            get
            {

                if (cart != null)
                {
                    return cart;
                }
                else
                {
                    return cart = new CartModel().GetCartByCartID(CartId);
                }

            }

        }


        List<CartLine> cartLine;
        public List<CartLine> CartLine
        {
            get
            {

                if (cartLine != null)
                {
                    return cartLine;
                }
                else
                {
                    return cartLine = new CartLineModel().GetCartLineByCartID(CartId);
                }

            }

        }
    }
}