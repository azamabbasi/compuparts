﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.ViewModel
{
    public class ProductDetailViewModel
    {
        int ProductID = Convert.ToInt32(HttpContext.Current.Request.RequestContext.RouteData.Values["ID"].ToString());

        Product product;
        public Product Product
        {
            get
            {
                if (product != null)
                {
                    return product;
                }
                else
                {
                    return product = new ProductModel().GetProductByID(ProductID);
                }
            }
        }

        List<Brand> brandList;
        public List<Brand> BrandList
        {
            get
            {
                if (brandList != null)
                {
                    return brandList;
                }
                else
                {
                    return brandList = new BrandModel().GetRandomBrands().ToList();
                }
            }
        }

        List<Product> productList;
        public List<Product> ProductList
        {
            get
            {
                if (productList != null)
                {
                    return productList;
                }
                else
                {
                    return productList = new ProductModel().GetRandomProduct().ToList();
                }
            }
        }

    }

    public class Quantitycheck
    {
        public int Size { get; set; }
        public int Colour { get; set; }
        public int ProductID { get; set; }

    }

    public class CartData
    {
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public decimal ProductPrice { get; set; }
    }
}