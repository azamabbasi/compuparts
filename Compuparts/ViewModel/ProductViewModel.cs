﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compuparts.ViewModel
{
    public class ProductProperties
    {
        public int ID { get; set; }
        public bool IsSelected { get; set; }
        public string Text { get; set; }
    }
    public class ProductViewModel
    {
        List<Category> listOfCategory;
        public List<Category> ListOfCategory
        {
            get
            {
                if (listOfCategory != null)
                {
                    return listOfCategory;
                }
                else
                {
                    return listOfCategory = new CategoryModel().GetAllInSameLine();
                }
            }
        }

        List<SelectListItem> listOfBrand;
        public List<SelectListItem> ListOfBrand
        {
            get
            {
                if (listOfBrand != null)
                {
                    return listOfBrand;
                }
                else
                {
                    return listOfBrand = new BrandModel().GetAll().Select(x => new SelectListItem() { Text = x.Name, Value = x.ID.ToString() }).ToList();
                }
            }
        }
        public Product Product { get; set; }

        public string ImagesHtml{ get; set; }

        public int ProductId
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Request.QueryString["ID"]);
            }
        }




       
        List<int> SelectedRegionIds
        {
            get
            {
                return new ProductRegionsModel().GetByProductID(ProductId).Select(s => s.RegionID).ToList();
            }
        }

        List<ProductProperties> regionList;

        public List<ProductProperties> RegionList
        {
            get
            {
                if (regionList != null)
                {
                    return regionList;
                }
                else
                {
                    return regionList = ListOfRegion.Select(l => new ProductProperties() { ID = l.ID, Text = l.Name.ToString(), IsSelected = SelectedRegionIds.Contains(l.ID) }).ToList();
                }
            }

        }

        List<Region> listOfRegion;
        public List<Region> ListOfRegion
        {
            get
            {
                if (listOfRegion != null)
                {
                    return listOfRegion;
                }
                else
                {
                    return listOfRegion = new RegionModel().GetAll();
                }
            }
        }

    










    }
}