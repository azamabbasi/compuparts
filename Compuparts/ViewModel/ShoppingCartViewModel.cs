﻿using Compuparts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compuparts.ViewModel
{
    public class ShoppingCartViewModel
    {
        int CartID
        {
            get
            {

                return HttpContext.Current.Session["Cartid"] != null ? Convert.ToInt32(HttpContext.Current.Session["Cartid"]) : 0;

            }
        }

        public int shippingPriceIreland = 0;
        public int shippingPriceInterNational = 0;
        public int IrelandMoreThen = 0;
        public int InterNationalMoreThen = 0;

        public string PaymentMethod { get; set; }

        List<CartLine> cartProducts;
        public List<CartLine> CartProducts
        {
            get
            {
                if (cartProducts != null)
                {
                    return cartProducts;
                }
                else
                {
                    return cartProducts = new CartLineModel().GetCartLineByCartID(CartID).ToList();
                }
            }
        }


        List<Region> region;
        public List<Region> Region
        {
            get
            {
                if (region != null)
                {
                    return region;
                }
                else
                {
                    return region = new RegionModel().GetAll().ToList();
                }
            }
        }

        List<Product> randomProducts;
        public List<Product> RandomProducts
        {
            get
            {
                if (randomProducts != null)
                {
                    return randomProducts;
                }
                else
                {
                    return randomProducts = new ProductModel().GetRandomProduct().ToList();
                }
            }
        }


        List<Setting> settings;
        public List<Setting> Settings
        {
            get
            {
                if (settings != null)
                {
                    return settings;
                }
                else
                {
                     settings = new SettingsModel().GetAllSettings();
                     shippingPriceIreland = Convert.ToInt32(settings.Where(x => x.Title == "ShippingIreland").Select(x => x.Value).FirstOrDefault());
                     shippingPriceInterNational = Convert.ToInt32(settings.Where(x => x.Title == "ShippingInternational").Select(x => x.Value).FirstOrDefault());
                     IrelandMoreThen = Convert.ToInt32(settings.Where(x => x.Title == "IrelandMoreThen").Select(x => x.Value).FirstOrDefault());
                     InterNationalMoreThen = Convert.ToInt32(settings.Where(x => x.Title == "InternationalMoreThen").Select(x => x.Value).FirstOrDefault());
                     return settings;
                }
            }
        }


        public BillingInfo billinfo { get; set; }

    }
}